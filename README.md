# Africartes DevOps resources

This repository contains [Kubernetes](https://kubernetes.io/) resource manifests.

## Configuration

[Install kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) and download your kubeconfig YAML file and export the `KUBECONFIG` env var from the cluster provider ([Scaleway](https://console.scaleway.com/)).

## Cluster setup

See <https://gitlab.com/jailbreak/infrastructure/kubernetes-cluster-management>

## Web API

```bash
kubectl create namespace afd-africartes

# Replace the "REPLACE_ME" placeholders in s3-bucket-credentials-secret.template.yaml, then:
kubectl apply -f africartes-api/s3-bucket-credentials-secret.template.yaml
# Warning: do not commit the secret values!
```

The other resources can be applied like this:

```bash
kubectl apply -k africartes-api
```

## Frontend

It's deployed on Netlify, not Kubernetes.
